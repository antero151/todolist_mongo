from pymongo import MongoClient
from bson.objectid import ObjectId

# class for work with mongodb
class TodoListDBModel:

    def __init__(self) -> None:
        # connecting to mongo and init collection
        self._client = MongoClient('mongo', 27017)
        self._db = self._client.todo
        self._todolists = self._db.todolists

    # get list of todo list
    def get_all_todolists(self):
        todo_lists = [self.clean_mongo_id(i) for i in self._todolists.find({})]
        return todo_lists

    # add new todolist
    def add_todolist(self, todolist):
        todolist['id'] = self._generate_list_id()
        todolist['todos'] = []
        new_todolist_id = self._todolists.insert_one(todolist).inserted_id
        return new_todolist_id

    #update todo list name
    def update_todolist(self, id, name):
        result = self._todolists.update_one({'id': int(id)}, {"$set": {"name": name}})
        return result.matched_count

    # remove todo list from db
    def delete_todolist(self, id):
        result = self._todolists.delete_one({'id': int(id)})
        return result.deleted_count

    # select one todo list by mongo _id
    def get_todolist_by_mongo_id(self, id):
        todolist = self._todolists.find({"_id": ObjectId(id)})[0]
        self.clean_mongo_id(todolist)
        return todolist

    # add todo item to selected todo list
    def create_todo(self, todolist_id, text):
        todo = {'text': text, 'done': 0, 'id': self._generate_todo_id(todolist_id)}
        self._todolists.update_one({'id': int(todolist_id)}, {'$push': {'todos': todo}})
        return todo

    # update todo item list in list of todos
    def update_todo(self, todo):
        status = self._todolists.update({'id': todo['todolist'], 'todos.id': todo['id']}, {'$set': {'todos.$': todo}})
        return status

    # delete todo item from todos list
    def delete_todo(self, todo):
        status = self._todolists.update({'id': todo['todolist'], 'todos.id': todo['id']}, {'$pull': {'todos': {'id': todo['id']}}})
        return status

    #remove mongo _id object for serialization
    def clean_mongo_id(self, obj):
        obj.pop("_id")
        return obj

    # genegate internal id for todo list
    def _generate_list_id(self):
        if self._todolists.count_documents({}) > 0:
            return self._todolists.find({}).sort([('id', -1)]).limit(1)[0].get("id") + 1
        else:
            return 1

    # genegate internal id for todo item
    def _generate_todo_id(self, todolist_id):
        pipeline = [
            {'$unwind': '$todos'},
            {'$match': {'id': int(todolist_id)}},
            {'$sort': {'todos.id': -1}},
            {'$limit': 1}

        ]
        todolist = [i for i in self._todolists.aggregate(pipeline)]
        if len(todolist) > 0:
            return todolist[0].get('todos').get('id') + 1
        else:
            return 1

    def _delete_all(self):
        self._todolists.delete_many({})