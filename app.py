from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
from models import TodoListDBModel

app = Flask(__name__)
CORS(app)
api = Api(app)
parser = reqparse.RequestParser()

todolist_model = TodoListDBModel()


# class for handling http request for getting and create todo list
class TododLists(Resource):

    def get(self):
        todolists = todolist_model.get_all_todolists()
        return todolists

    def post(self):
        new_todolist_id = todolist_model.add_todolist(request.get_json(force=False))
        response = todolist_model.get_todolist_by_mongo_id(new_todolist_id)
        return response


# class for handling http request for update and delete todo list
class TododList(Resource):

    def put(self, todolist_id):
        status = todolist_model.update_todolist(todolist_id, request.get_json(force=False).get('name'))
        if status == 1:
            return {'status': 'updated'}
        else:
            return {'status': 'not updated'}

    def delete(self, todolist_id):
        status = todolist_model.delete_todolist(todolist_id)
        if status == 1:
            return {'status': 'deleted'}
        else:
            return {'status': 'not deleted'}


# class for handling http request for create, update and delete todo item in
class Todo(Resource):

    def post(self):
        response_json = request.get_json(force=False)
        todo = todolist_model.create_todo(response_json.get('todolist'), response_json.get('text'))
        return todo

    def put(self):
        response_json = request.get_json(force=False)
        todolist_model.update_todo(response_json)
        return response_json

    def delete(self):
        response_json = request.get_json(force=False)
        todolist_model.delete_todo(response_json)
        return response_json


# routing
api.add_resource(TododLists, '/api/todolists')
api.add_resource(TododList, '/api/todolist/<todolist_id>')
api.add_resource(Todo, '/api/todo')


if __name__ == '__main__':
    app.run(debug=True, port=5000, host="0.0.0.0")
