ToDo List (Flask, Mongo, Vue js)
================================
Instalation and launch (Linux/Mac os)
--------------------------------
First you need to install [Docker-compose](https://docs.docker.com/compose/install/) 

Then clone repo and enter to the dir `cd todolist_mongo `

And type in console:

`chmod +x run.sh`

`./run.sh`

You app will be able on <http://localhost:8080/>
**************************************
###Launch single service
**************************************
####mongoBD

`docker-compose up mongo --build`
----------------
####fask app

`docker-compose run server pip install -r requirements.txt`

`docker-compose up server --build`
----------------

####Vue js app

`docker-compose run client npm i`

`docker-compose up client --build`
-----------------